use std::{
    collections::HashSet,
    env, fs,
    os::{
        linux::fs::MetadataExt,   // Read file UID, GID
        unix::fs::PermissionsExt, // Read file permissions
    },
    path::{Path, PathBuf},
};

use color_eyre::{
    Result,
    eyre::{ContextCompat, WrapErr},
};
use crossterm::event::{self, Event, KeyCode, KeyEvent, KeyEventKind};
use ratatui::{
    prelude::*,
    style::Stylize,
    symbols::border,
    widgets::{block::*, *},
};
use users::{get_current_uid, get_user_by_uid, os::unix::UserExt}; // Read process UID, GID

mod errors;
mod tui;

#[derive(Debug, Default, Eq, PartialEq)]
enum State {
    #[default]
    Normal,
    Renaming,
    Input,
    Prompt(Type),
}

#[derive(Debug, PartialEq, Eq)]
enum Type {
    Hover,
    Sel,
}

#[derive(Clone, Debug, Default, PartialEq)]
enum SortDirection {
    #[default]
    Normal,
    Rev,
}

#[derive(Debug, Default)]
struct Input {
    value:  String,
    cursor: usize,
}

#[derive(Debug, Default)]
pub struct App {
    tabs: Vec<Tab>,

    // global across all tabs and pages
    tab_idx:     usize,     // 3 file tabs, 1 page with keybindings
    loaded_tabs: Vec<bool>, // which tabs have been loaded
    size:        Rect,      // frame size
    sel:         HashSet<PathBuf>,
    marked:      Option<PathBuf>,
    state:       State,
    input:       Input,
    prompt:      Option<bool>,
    exit:        bool,
}

#[derive(Clone, Debug, Default)]
struct Tab {
    hov_idx:        usize,        // where we are currently hovering on the list
    cur_dir:        PathBuf,      // current dir (parent of any in items)
    show_hidden:    bool,         // show hidden files
    items:          Vec<PathBuf>, // content of current dir
    sort_direction: SortDirection,
}

impl Tab {
    fn init() -> Result<Tab> {
        Ok(Tab {
            hov_idx:        0,
            cur_dir:        env::current_dir().wrap_err("no cwd")?,
            show_hidden:    false,
            items:          vec![],
            sort_direction: SortDirection::Normal,
        })
    }
}

impl App {
    // runs the application's main loop until the user quits
    pub fn run(&mut self, terminal: &mut tui::Tui) -> Result<()> {
        // set startup values
        self.tabs = vec![Tab::init()?; 5];

        self.prompt = None;
        self.sel = HashSet::new();
        self.tab_idx = 0;
        self.loaded_tabs = vec![true, false, false, false, false];

        // populate initial dir and its content
        self.find_items()?;

        while !self.exit {
            terminal.draw(|frame| self.render_frame(frame))?;
            self.handle_events().wrap_err("handle events failed")?;
        }
        Ok(())
    }

    fn render_frame(&mut self, frame: &mut Frame) {
        self.size = frame.area();
        frame.render_widget(&*self, frame.area());
    }

    fn handle_events(&mut self) -> Result<()> {
        if self.prompt == Some(true) {
            match self.state {
                State::Prompt(Type::Hover) => self.delete_item(false)?,
                State::Prompt(Type::Sel) => self.delete_item(true)?,
                _ => {}
            }
            self.prompt = None;
            self.state = State::Normal;
            return Ok(());
        } else if self.prompt == Some(false) {
            self.prompt = None;
            self.state = State::Normal;
            return Ok(());
        }

        let _ = match event::read()? {
            // it's important to check that the event is a key press event as
            // crossterm also emits key release and repeat events on Windows.
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => match self.state {
                State::Normal => self
                    .handle_key_event(key_event)
                    .wrap_err_with(|| format!("handling key event failed:\n{key_event:#?}")),
                State::Input | State::Renaming => {
                    self.handle_input_event(key_event);
                    Ok(())
                }
                State::Prompt(_) => {
                    if let Some(val) = self.handle_prompt(key_event) {
                        self.prompt = Some(val);
                    };
                    Ok(())
                }
            },
            _ => Ok(()),
        };
        Ok(())
    }

    fn handle_key_event(&mut self, key_event: KeyEvent) -> Result<()> {
        match key_event.code {
            KeyCode::Char('q') => self.close(), // close current tab and then exit
            KeyCode::Char('§') => self.exit = true, // force close
            KeyCode::Char('r') => self.input_mode(true),
            KeyCode::Char('f') => self.find_items()?, // refresh items in current dir
            KeyCode::Char('h') => self.toggle_show_hidden()?, // show hidden items
            KeyCode::Char('H') => self.go_home()?,    // show hidden items
            KeyCode::Char('s') => self.change_sorting(),
            KeyCode::Char('g') => self.tabs[self.tab_idx].hov_idx = 0,
            KeyCode::Char('G') => {
                self.tabs[self.tab_idx].hov_idx = self.tabs[self.tab_idx].items.len() - 1
            }
            KeyCode::Char('1') => self.change_window(0),
            KeyCode::Char('2') => self.change_window(1),
            KeyCode::Char('3') => self.change_window(2),
            KeyCode::Char('4') => self.change_window(3),
            KeyCode::Char('5') => self.change_window(4),
            KeyCode::Char(' ') => self.change_sel(),
            KeyCode::Char('n') => self.create_item()?,
            KeyCode::Char('d') => self.state = State::Prompt(Type::Hover),
            KeyCode::Char('D') => {
                if !self.sel.is_empty() {
                    self.state = State::Prompt(Type::Sel);
                }
            }
            KeyCode::Char('M') => self.mark_dest(),
            KeyCode::Left => self.ascend()?,   // move to parent dir
            KeyCode::Right => self.descend()?, // move into dir
            KeyCode::Up => self.move_up(),
            KeyCode::Down => self.move_down(),
            _ => (),
        }
        Ok(())
    }

    fn handle_prompt(&mut self, key_event: KeyEvent) -> Option<bool> {
        match key_event.code {
            KeyCode::Char('y') | KeyCode::Char('Y') => Some(true),
            KeyCode::Char('n') | KeyCode::Char('N') => Some(false),
            _ => None,
        }
    }

    fn input_mode(&mut self, rename: bool) {
        if self.is_tab_empty() && rename {
            return;
        };

        self.state = if rename { State::Renaming } else { State::Input };

        if rename {
            let filename = self
                .cur_item()
                .file_name()
                .unwrap()
                .to_string_lossy()
                .to_string();
            self.input.value = filename;

            self.input.cursor = if self.input.value.starts_with('.')
                && self.input.value.chars().filter(|c| *c == '.').count() != 1
            {
                // starts with . and there's a file extension
                self.input.value.rfind('.').unwrap()
            } else if self.input.value.starts_with('.') {
                self.input.value.len()
            } else if let Some(cursor) = self.input.value.rfind('.') {
                // not hidden, with file extension
                cursor
            } else {
                self.input.value.len()
            };
        } else {
            // general text input
            self.input.value = String::new();
            self.input.cursor = 0;
        }
    }

    fn handle_input_event(&mut self, key_event: KeyEvent) {
        if let Some(key) = match key_event.code {
            KeyCode::Esc => {
                self.input.value = String::new();
                if self.state == State::Input {
                    self.move_up();
                }
                self.state = State::Normal;
                return;
            }
            KeyCode::Left => {
                self.input.cursor = self.input.cursor.saturating_sub(1);
                return;
            }
            KeyCode::Right => {
                if self.input.cursor < self.input.value.len() {
                    self.input.cursor += 1
                };
                return;
            }
            KeyCode::Backspace => {
                if self.input.cursor > 0 {
                    _ = self.input.value.remove(self.input.cursor - 1);
                    self.input.cursor = self.input.cursor.saturating_sub(1);
                }
                return;
            }
            KeyCode::Enter => {
                if self.state == State::Input {
                    if self.input.value.is_empty() {
                        self.state = State::Normal;
                        self.move_up();
                        return;
                    }

                    let mut new_item = PathBuf::from(&self.tabs[self.tab_idx].cur_dir);
                    if self.input.value.ends_with("/") {
                        new_item.push(self.input.value.trim_end_matches('/'));
                        fs::create_dir(&new_item).unwrap_or_else(|_| self.move_up());
                    } else {
                        new_item.push(&self.input.value);
                        let file = fs::File::create(&new_item);
                        if file.is_err() {
                            self.move_up();
                        }
                    }

                    _ = self.find_items();
                } else {
                    if self.input.value.is_empty() {
                        return;
                    }
                    let old_name = &self.cur_item();
                    let cur_tab = &mut self.tabs[self.tab_idx];
                    cur_tab.items[cur_tab.hov_idx].set_file_name(&self.input.value);
                    let new_name = &self.cur_item();

                    _ = fs::rename(old_name, new_name);
                }

                self.state = State::Normal;
                return;
            }
            KeyCode::Char(c) => Some(c),
            _ => None,
        } {
            self.input.value.insert(self.input.cursor, key);
            self.input.cursor += 1;
        };
    }

    fn find_items(&mut self) -> Result<()> {
        self.tabs[self.tab_idx].items.clear();
        let paths = fs::read_dir(&self.tabs[self.tab_idx].cur_dir)?;

        for path in paths {
            let tmp = path?.path();

            if self.tabs[self.tab_idx].show_hidden
                || !tmp
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string()
                    .starts_with('.')
            {
                self.tabs[self.tab_idx].items.push(tmp);
            };
        }

        // sort alph and dirs on top
        self.sort_items();
        Ok(())
    }

    fn go_home(&mut self) -> Result<()> {
        let user = get_user_by_uid(get_current_uid()).wrap_err("no user")?;
        let home_dir = user.home_dir();

        self.tabs[self.tab_idx].cur_dir = home_dir.to_path_buf();
        self.find_items()?;
        self.tabs[self.tab_idx].hov_idx = 0;
        Ok(())
    }

    fn create_item(&mut self) -> Result<()> {
        self.state = State::Input;
        self.input_mode(false);

        self.tabs[self.tab_idx].hov_idx = self.tabs[self.tab_idx].items.len();
        Ok(())
    }

    fn delete_item(&mut self, selection: bool) -> Result<()> {
        if !self.sel.is_empty() && selection {
            for item in &self.sel {
                if item.is_file() {
                    fs::remove_file(item)?;
                } else if item.is_dir() {
                    fs::remove_dir_all(item)?;
                }
            }
        } else if self.cur_item().is_file() {
            fs::remove_file(self.cur_item())?;
        } else if self.cur_item().is_dir() {
            fs::remove_dir_all(self.cur_item())?;
        }

        self.find_items()?;
        self.prompt = None;
        self.state = State::Normal;
        Ok(())
    }

    fn mark_dest(&mut self) {
        if !self.cur_item().is_dir() {
        } else if Some(self.cur_item()) == self.marked {
            self.marked = None;
        } else {
            if self.sel.contains(&self.cur_item()) {
                _ = self.sel.remove(&self.cur_item());
            }
            self.marked = Some(self.cur_item());
        }
    }

    fn toggle_show_hidden(&mut self) -> Result<()> {
        self.tabs[self.tab_idx].show_hidden = !self.tabs[self.tab_idx].show_hidden;

        let old_len = self.tabs[self.tab_idx].items.len();
        let old_item = self.cur_item().to_owned();

        self.find_items()?;

        // Only move cursor if there are hidden items
        if self.tabs[self.tab_idx].items.len() != old_len {
            self.tabs[self.tab_idx].hov_idx = 0;

            for i in 0..self.tabs[self.tab_idx].items.len() {
                if self.tabs[self.tab_idx].items[i] == old_item {
                    self.tabs[self.tab_idx].hov_idx = i;
                }
            }
        }
        Ok(())
    }

    fn sort_items(&mut self) {
        // sort alph and dirs on top
        self.tabs[self.tab_idx]
            .items
            .sort_unstable_by_key(|a| a.to_string_lossy().to_string().to_lowercase());
        if self.tabs[self.tab_idx].sort_direction == SortDirection::Rev {
            self.tabs[self.tab_idx].items.reverse();
        }

        self.tabs[self.tab_idx]
            .items
            .sort_unstable_by_key(|p| !p.is_dir());
    }

    fn change_sorting(&mut self) {
        if self.is_tab_empty() {
            return;
        };

        let old_item = self.cur_item().to_owned();

        if self.tabs[self.tab_idx].sort_direction == SortDirection::Normal {
            self.tabs[self.tab_idx].sort_direction = SortDirection::Rev;
        } else {
            self.tabs[self.tab_idx].sort_direction = SortDirection::Normal;
        }

        self.sort_items();

        for i in 0..self.tabs[self.tab_idx].items.len() {
            if old_item == self.tabs[self.tab_idx].items[i] {
                // if where we ascended from is the item at index i...
                self.tabs[self.tab_idx].hov_idx = i; // ... hover there
            }
        }
    }

    fn change_window(&mut self, idx: usize) {
        // change cur_dir;
        if !self.loaded_tabs[idx] {
            self.loaded_tabs[idx] = true;
            self.tabs[idx].cur_dir = self.tabs[self.tab_idx].cur_dir.clone();
            self.tabs[idx].items = self.tabs[self.tab_idx].items.clone();
        }

        self.tab_idx = idx; // update current window to new index
    }

    fn close(&mut self) {
        // move to prev tab
        let mut can_close = true;
        let mut c = self.tab_idx;
        for i in (0..c).rev() {
            if self.loaded_tabs[i] {
                can_close = false;
                c = i;
                break;
            }
        }

        // move to next tab if prev not found
        if can_close == (self.tab_idx < self.loaded_tabs.len()) {
            c = self.tab_idx;
            for i in (c + 1)..self.loaded_tabs.len() {
                if self.loaded_tabs[i] {
                    can_close = false;
                    c = i;
                    break;
                }
            }
        }

        self.loaded_tabs[self.tab_idx] = false;
        self.change_window(c);

        // Check to only close program when only 1 tab is loaded
        if can_close {
            self.exit = true;
        }
    }

    fn change_sel(&mut self) {
        if self.is_tab_empty() || Some(self.cur_item()) == self.marked {
            return;
        }

        if !self.sel.insert(self.cur_item()) {
            _ = self.sel.remove(&self.cur_item());
        }
        self.move_down();
    }

    fn ascend(&mut self) -> Result<()> {
        let old_dir = self.tabs[self.tab_idx].cur_dir.to_owned(); // where we ascended from

        // ascend if not at root
        if self.tabs[self.tab_idx].cur_dir != PathBuf::from("/") {
            self.tabs[self.tab_idx].cur_dir = self.tabs[self.tab_idx]
                .cur_dir
                .parent()
                .wrap_err("no parent")?
                .to_path_buf();
        }

        self.find_items()?; // populate contents

        self.tabs[self.tab_idx].hov_idx = 0; // reset hov_idx if we don't find it below

        // find the index of parent in items
        for i in 0..self.tabs[self.tab_idx].items.len() {
            // iterate though all items
            if old_dir == self.tabs[self.tab_idx].items[i] {
                // if where we ascended from is the item at index i...
                self.tabs[self.tab_idx].hov_idx = i; // ... hover there
            }
        }
        Ok(())
    }

    fn descend(&mut self) -> Result<()> {
        if self.is_tab_empty() {
            return Ok(());
        };

        // get current users UID, GID
        let Some(user) = get_user_by_uid(get_current_uid()) else {
            // FIXME: Should probably return an Error but we just want the early return so who cares.
            return Ok(());
        };
        let cur_item = &self.cur_item();

        if cur_item.is_dir()
            && (
                // Not unreachable (700 != file perms)
                (*"40700" != format!("{:o}", cur_item.metadata()?.permissions().mode()))
                // if it's unreachable but owned by the process it's fine (process UID == file UID)
                || (user.uid() == cur_item.metadata()?.st_uid())
            )
        {
            // move into dir at index hov_idx
            self.tabs[self.tab_idx].cur_dir = cur_item.clone();

            // update hover index and populate current dir
            self.tabs[self.tab_idx].hov_idx = 0;
            self.find_items()?;
        }
        Ok(())
    }

    fn move_up(&mut self) {
        if self.is_tab_empty() {
            return;
        };

        if self.tabs[self.tab_idx].hov_idx != 0 {
            self.tabs[self.tab_idx].hov_idx -= 1;
        } else {
            self.tabs[self.tab_idx].hov_idx = self.tabs[self.tab_idx].items.len() - 1;
        }
    }

    fn move_down(&mut self) {
        if self.is_tab_empty() {
            return;
        };

        if self.tabs[self.tab_idx].hov_idx < self.tabs[self.tab_idx].items.len() - 1 {
            self.tabs[self.tab_idx].hov_idx += 1;
        } else {
            self.tabs[self.tab_idx].hov_idx = 0;
        }
    }

    fn is_tab_empty(&self) -> bool { self.tabs[self.tab_idx].items.is_empty() }

    fn cur_item(&self) -> PathBuf {
        self.tabs[self.tab_idx].items[self.tabs[self.tab_idx].hov_idx].clone()
    }
}

impl Widget for &App {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let cur_dir = &self.tabs[self.tab_idx].cur_dir;

        let title = fmt_cur_dir(cur_dir, area.width)
            .unwrap_or(Line::from(cur_dir.to_string_lossy().to_string()));

        let hidden = if self.tabs[self.tab_idx].show_hidden {
            String::from(" H")
        } else {
            String::new()
        };

        let direction = if self.tabs[self.tab_idx].sort_direction == SortDirection::Rev {
            String::from(" A/R")
        } else {
            String::from(" A")
        };

        let mut tabs = vec![];
        for i in 0..=4 {
            if i == self.tab_idx {
                tabs.push(format!("{}", i + 1).on_blue().white());
            } else if self.loaded_tabs[i] {
                tabs.push(format!("{}", i + 1).blue());
            } else {
                tabs.push(format!("{}", i + 1).into());
            };
            tabs.push(" ".into());
        }
        _ = tabs.pop(); // remove extra space on end

        let tab = Line::from(tabs);

        let tooltip = Line::from(vec![
            if self.state == State::Prompt(Type::Hover) {
                String::from(" DEL HOV").into()
            } else if self.state == State::Prompt(Type::Sel) {
                String::from(" DEL SEL").into()
            } else {
                String::new().into()
            },
            format!(
                " {}/{}",
                self.tabs[self.tab_idx].hov_idx + 1,
                self.tabs[self.tab_idx].items.len()
            )
            .into(),
            hidden.into(),
            direction.into(),
        ]);

        let block = Block::default()
            .title_top(tab)
            .title_top(title)
            .title_bottom(tooltip.right_aligned())
            .borders(Borders::BOTTOM)
            .border_set(border::THICK);

        let items_text = display_in_range(self);

        Paragraph::new(items_text)
            .alignment(Alignment::Left)
            .block(block)
            .render(area, buf);
    }
}

fn fmt_cur_dir(p: &Path, w: u16) -> Result<Line> {
    let width = w - 13; // width of tabs + prefixed ellipses
    let mut path = p.to_string_lossy().to_string();

    let home_dir = env::home_dir()
        .wrap_err("no home dir")?
        .to_string_lossy()
        .to_string();

    if path.starts_with(&home_dir) {
        path = path.replace(&home_dir, "~");
    };

    if path.len() >= width as usize {
        path = path
            .chars()
            .take(path.len())
            .skip(path.len() - width as usize)
            .collect();
        path.insert_str(0, "...");
    }

    Ok(Line::from(path).bold())
}

fn display_in_range(app: &App) -> Text {
    let height = usize::from(app.size.height) - 2; // window height
    let mut lines: Vec<Line> = vec![];

    let offset = (app.tabs[app.tab_idx].hov_idx / height) * height;

    for i in offset..app.tabs[app.tab_idx].items.len() {
        // only display filenames, not complete path
        let Some(text_osstr) = app.tabs[app.tab_idx].items[i].file_name() else {
            continue;
        };
        let text = text_osstr.to_string_lossy().to_string();

        let mut style = Style::new();
        let mut prefix = String::from(" ");
        let mut postfix = String::new();

        if app.tabs[app.tab_idx].items[i].is_dir() {
            style = style.blue();
            postfix = String::from("/");
        } else {
            style = style.white();
        };

        if app.marked == Some(app.tabs[app.tab_idx].items[i].clone()) {
            prefix = String::from("M");
        } else if app.sel.contains(&app.tabs[app.tab_idx].items[i]) {
            prefix = String::from("*");
        }
        // highlight items prompting for deletion
        if app.state == State::Prompt(Type::Sel)
            && app.sel.contains(&app.tabs[app.tab_idx].items[i])
        {
            style = style.on_red();
        }

        // highlight current hovered over item
        if i == app.tabs[app.tab_idx].hov_idx {
            if app.state == State::Renaming {
                lines.push(line_with_cursor(app));
                continue;
            } else if app.state == State::Prompt(Type::Hover) {
                style = style.on_red();
            } else {
                style = style.reversed();
            }
        }
        lines.push(Line::from(vec![prefix.into(), text.into(), postfix.into()]).style(style));
    }

    if app.state == State::Input {
        lines.push(line_with_cursor(app));
    }

    Text::from(lines)
}

fn line_with_cursor(app: &App) -> Line {
    let mut text = String::from(&app.input.value);

    // add whitespace so cursor can go off the word end
    text.push(' ');

    // Add cursor highlight
    let mut cur_char = text.split_off(app.input.cursor);
    let after = cur_char.split_off(1);

    let cur_item = app.tabs[app.tab_idx].cur_dir.join(&app.input.value);
    let style = if cur_item.is_dir() && app.state == State::Renaming {
        Style::new().blue()
    } else {
        Style::new().white()
    };

    let prefix = Span::from(" ").style(style);

    Line::from(vec![
        prefix,
        Span::styled(text, style),
        Span::styled(cur_char, style.reversed()),
        Span::styled(after, style),
    ])
}

fn main() -> Result<()> {
    errors::install_hooks()?;
    let mut terminal = tui::init()?;
    App::default().run(&mut terminal)?;
    tui::restore()?;
    Ok(())
}
