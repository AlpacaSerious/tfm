# tfm
Another TUI File Manager

## TODO:
- [ ] File operations:
  - [ ] Move
  - [ ] Copy
  - [x] Create
  - [x] Delete (single, bulk)
- [ ] File renamer:
  - [x] single file mode
  - [ ] bulk
- [ ] More sort modes (size, date)
- [ ] Line display stats
- [ ] Key bindings page
- [ ] Maybe settings page, with config file?
- [ ] Bookmarking dirs: saving with a name, reloading, resetting
- [ ] Split screen?
